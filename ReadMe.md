#What is Verify Hash?
VerifyHash is an AppleScript which makes it easy to verify a file hash, also known as a message digest or checksum. You can check the authenticity of a downloaded file by comparing the hash given with the file's hash.

#Requirements
AppleScripts run on the Mac platform, and this script should run on all versions of Mac OS X. 

#Installation
AppleScripts do not need to be installed. Clone the repo or download the zip and extract the .applescript file. Open the file and save as a Script or Application, according to your preference.

#Issues
Report problems on the [issues tracking page](https://github.com/seesolve/VerifyHash/issues).

#License
[MIT License](https://github.com/seesolve/VerifyHash/blob/master/LICENSE).
