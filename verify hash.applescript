set textButtonCancel to "Cancel"
set textButtonOkay to "Okay"
set textButtonNext to "Next"
set textButtonYes to "Yes"
set textButtonNo to "No"

set textClipboard to the clipboard

set textIntro to "This script checks the authenticity of a file by verifying that the hash provided matches the file's hash." & return & return



-- ask user for hash; script automatically copies clipboard, in case hash is already there
set listDialogResults to display dialog textIntro & "The clipboard contents are displayed below. If what is displayed is not the hash, paste the hash provided in the text field below." default answer textClipboard buttons {textButtonCancel, textButtonNext} default button textButtonNext cancel button textButtonCancel with title "Enter Hash"



-- ask user for file to verify; first check if file is already selected in Finder
tell application "Finder"
	set aliasFileSelected to selection as alias
	
	set textFileConfirmed to button returned of (display dialog "Are we checking the hash for this file you have selected in Finder?" & return & return & aliasFileSelected as text buttons {textButtonCancel, textButtonNo, textButtonYes} default button textButtonYes cancel button textButtonCancel with title "Confirm File")
	
	
	if textFileConfirmed is "No" then
		set aliasFileSelected to choose file with prompt "Select the file whose hash you want to verify" default location aliasFileSelected with invisibles
	end if
end tell

-- get file path as posix (to use in shell script)
set textFilePosixPath to POSIX path of aliasFileSelected



-- ask user for hash type
set textHashType to item 1 of (choose from list {"MD2", "MD5", "MDC2", "RMD-160", "SHA", "SHA-1", "SHA-224", "SHA-256", "SHA-384", "SHA-512"} with title "Select Hash Type" with prompt "Select the type of hash" default items {"SHA-256"} OK button name textButtonOkay cancel button name textButtonCancel without multiple selections allowed and empty selection allowed)

-- change hash type to lowercase and remove dash if exists (to use in shell script)
set textHashOption to do shell script "echo" & space & quoted form of textHashType & " | " & "tr [:upper:] [:lower:]" & " | " & "tr -d '-'"



-- use shell script to get file hash
set textResults to do shell script "openssl" & space & "dgst" & space & "-" & textHashOption & space & quoted form of textFilePosixPath



-- parse formatted results for hash only
set textMsgDigestCalc to word -1 of textResults



-- check results and notify user
if textMsgDigestCalc = textClipboard then
	display dialog textHashType & space & "hashes match. File is authentic." buttons {textButtonOkay} default button textButtonOkay with title "File Verified" with icon note
else
	display dialog textHashType & space & "hashes do not match. File is not verified and may not be authentic." buttons {textButtonOkay} default button textButtonOkay with title "File Not Verified" with icon caution
end if



